const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        const { id, name, power, health, defense } = req.body;
        
        if(id) {
            throw new Error('Reqest body has "id" property!');
        }
        
        if(!name) {
            throw new Error('Some field is empty or bed!');
        }

        if(power <= 0 || power > 100) {
            throw new Error('Power is bed!');
        }

        if(health <= 80 || health > 120) {
            throw new Error('Health is bed!');
        }

        if(defense < 1 || defense > 10 ) {
            throw new Error('Defense is bed!');
        }

        if(!Number.isInteger(power) || !Number.isInteger(defense) || !Number.isInteger(health)) {
            throw new Error('Power or Defense must be an integer!');
        }

        if(Object.keys(req.body).length == 0) {
            throw new Error('Data is empty');
        }

        next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);
        const fighterKeys = Object.keys(fighter);

        if(!fields.every(key => fighterKeys.indexOf(key) >= 0) || req.body.id || FighterService.findFighterById(id) || Object.keys(fields).length == 0) {
            throw new Error('Fighter Validation or data processing error');
        } else
            for(let key in req.body) {
                if(key === 'health' && req.body[key] < 0) {
                    throw new Error('Fighter Validation or data processing - health');
                    break;
                }

                if(key === 'defense' && (req.body[key] < 1 || req.body[key] > 10 || isNaN(req.body[key]))) {
                    throw new Error('Fighter Validation or data processing - defense');
                    break;
                }
                
                if(key === 'power' && (req.body[key] <= 0 || req.body[key] >= 100 || isNaN(req.body[key]))) {
                    throw new Error('Fighter Validation or data processing - power');
                    break;
                }
            }
            next();
    } catch(e) {
        res.status(400).json(e.message);
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
