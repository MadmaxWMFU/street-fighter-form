const { user } = require('../models/user');
const emailTest = new RegExp(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/);
const numberTest = new RegExp(/^\+?([3][8][0])\)?([0-9]{9})$/);

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        const {id, firstName, lastName, email, phoneNumber, password } = req.body;
       
        if(id) {
            throw new Error('Reqest body has "id" property!');
        }

        if(!firstName || !lastName ) {
            throw new Error('FirstName or LastName entered incorrectly!');
        }

        if(!email || !emailTest.test(email) || !email.includes('@gmail.com')) {
            throw new Error('Email entered incorrectly!');
        }

        if(!phoneNumber || !numberTest.test(phoneNumber)) {
            throw new Error('Number entered incorrectly');
        }

        if (!password || !password.length >= 3) {
            throw new Error('Password entered incorrectly')
        } 
        
        if(Object.keys(req.body).length == 0) {
            throw new Error('Data is empty');
        }
        next();
    }catch (e) {
        res.status(400).json(e.message);
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);

        if (!fields.every(key => userKeys.indexOf(key) >= 0) || id || !UserService.findUsersById(id) || Object.keys(fields).length == 0) {
            throw new Error('Validation or data processing error-1');
        } else {
            for (let key in req.body) {
                if (key === 'email' && (!req.body[key].includes('@gmail.com') || !emailTest.test(req.body[key]))) {
                    throw new Error('Validation or data processing error-2')
                    break;
                }
                if (key === 'password' && req.body[key].length < 3) {
                    throw new Error('Validation or data processing error-3')
                    break;
                }
                if (key === 'phoneNumber' && !numberTest.test(req.body[key])) {
                    throw new Error('Validation or data processing error-4')
                    break;
                }
            }
            next();
        }
    }catch (e) {
        res.status(400).json(e.message);
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
