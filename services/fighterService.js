const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();
        if(!fighters) {
            throw Error('No fighters in db');
        }
        return fighters;
    }
  
    create(fighter) {
        let created = FighterRepository.create(fighter);
        if(!created) {
            throw Error('Fighter not created');
        }
        return created;
    }

    delete(id){
        const removed = FighterRepository.delete(id);
        if(!removed) {
            throw Error('Fighter not removed');
        }
        return removed;
    }
  
    update(id, info){
        const result = FighterRepository.update(id, info);
        if(!result) {
            throw Error('Fighter not updated');
        }
        return result;
    }

    getFighterById(id) {
        const search = this.search({ id })
        
        if(!search) {
            throw Error('User not found');
        }
        
        return search;
    }
    
    search(search) {
        const fighter = FighterRepository.getOne(search);
        if(!fighter) {
            throw Error('Fighter not found');
        }
        return fighter;
    }  
}

module.exports = new FighterService();
