const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    create(user) {
        let created = UserRepository.create(user);
        
        if(!created) {
            throw Error('User not created');
        }
        
        return created;
    }

    getUsers() {
        const users = UserRepository.getAll();
        
        if(!users) {
            throw Error('No users in db');
        }

        return users;
    }

    delete(id) {
        const user = UserRepository.delete(id);
        
        if(!user) {
            throw Error('User not found');
        }
        
        return user;
    }

    update(id, data) {
        const result = UserRepository.update(id, data);
        
        if(!result) {
          throw Error('User not found');
        }
        
        return result;
    }

    getUserById(id) {
        const search = this.search({ id })
        
        if(!search) {
            throw Error('User not found');
        }
        
        return search;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        
        if(!item) {
            return null;
        }
        
        return item;
    }
}

module.exports = new UserService();
